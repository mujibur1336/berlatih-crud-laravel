<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){

        //dd($request->all());
        $validatedData = $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio"  => $request["bio"]

        ]);

        return redirect('cast/create');
    }

    public function index(){
        $cast = DB::table('cast')->get();
        return view('cast.index', compact('cast'));
    }
    public function show($cast_id){
        $cast = DB::table('cast')->where('id',$cast_id)->first();
        return view('cast.show', compact('cast'));
    }
    public function edit($cast_id){
        $cast = DB::table('cast')->where('id',$cast_id)->first();
        return view('cast.edit', compact('cast'));
    }
    public function update($cast_id, Request $request){

        //dd($request->all());
        $affected = DB::table('cast')
              ->where('id', $cast_id)
              ->update([
                "nama" => $request["nama"],
                "umur" => $request["umur"],
                "bio"  => $request["bio"]
                ]);

        return redirect('/cast');
    }
    public function delete($cast_id){
        $cast = DB::table('cast')->where('id',$cast_id)->delete();
        return redirect('/cast');
    }
}
