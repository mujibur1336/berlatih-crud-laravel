@extends('adminlte.master')
@section('content')
<div class="ml-3 mt-3">
<div class="card card-primary">
    <div class="card-header">
    <h3 class="card-title">Create Cast</h3>
    </div>
    <form action="/cast" method="POST">
    @csrf 
    <div class="card-body">
    <div class="form-group">
    <label for="exampleInputNama">Nama</label>
    <input type="text" class="form-control" id="exampleInputNama" name="nama" placeholder="Ente Name">
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>

    <div class="form-group">
    <label for="exampleInputUmur">Umur</label>
    <input type="number" class="form-control" id="exampleInputEmail1" name="umur" placeholder="Enter Umur">
    @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div> 
    <div class="form-group">
        <label for="exampleInputUmur">Bio</label>
        <textarea class="form-control" name="bio" rows="3" placeholder="Enter ..."></textarea>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        </div> 
    </div>
    
    <div class="card-footer">
    <button type="submit" class="btn btn-primary">Submit</button>
    </div>
    </form>
    </div>
</div>
@endsection